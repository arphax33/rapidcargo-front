import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import {Skeleton} from "primeng/skeleton";

@Directive({
  selector: '[ngSkeleton]'
})
export class SkeletonDirective implements OnInit, OnChanges {
  @Input() ngSkeleton: boolean

  height: string;
  width: string;

  constructor(private element: ElementRef<HTMLElement>,
              private template: TemplateRef<any>,
              private view: ViewContainerRef) { }

  ngOnInit() {
    const embed = this.view.createEmbeddedView(this.template)
    this.height = embed.rootNodes[0].offsetHeight > 0 ? embed.rootNodes[0].offsetHeight + 'px' : 1 + 'rem'
    this.width = embed.rootNodes[0].offsetWidth > 0 ? embed.rootNodes[0].offsetWidth + 'px' : 1 + 'rem'

    this.toggleSkeleton()
  }

  ngOnChanges() {
    this.toggleSkeleton()
  }

  private toggleSkeleton() {
    this.view.clear();

    if (!this.ngSkeleton) {
      const component = this.view.createComponent(Skeleton)

      component.instance.width = this.width
      component.instance.height = this.height
      component.instance.styleClass = 'm-1'
    } else {
      this.view.createEmbeddedView(this.template)
    }
  }
}
