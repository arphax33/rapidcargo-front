export enum WarehouseMovementType {
  OUTPUT = "OUTPUT",
  INPUT = "INPUT"
}
