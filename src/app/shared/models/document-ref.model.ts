export class DocumentRef {
  type: DocumentType;
  reference: string;

  constructor(type: DocumentType, reference: string) {
    this.type = type;
    this.reference = reference;
  }
}
