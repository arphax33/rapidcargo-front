import { Component } from '@angular/core';

@Component({
  selector: 'rapidcargo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'RapidCargo';
}
