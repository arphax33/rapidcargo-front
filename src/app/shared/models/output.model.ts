import {WarehouseMovement} from "@shared/models/warehouse-movement.model";
import {Place} from "@shared/models/place.model";
import {DocumentRef} from "@shared/models/document-ref.model";

export class OutputMovement extends WarehouseMovement {
  documentRef: DocumentRef;
}
