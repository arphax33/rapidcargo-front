import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import {WarehouseMovementType} from "@shared/enums/warehouse-movement-type.enum";
import {InputMovement} from "@shared/models/input.model";
import {OutputMovement} from "@shared/models/output.model";
import {WarehouseMovement} from "@shared/models/warehouse-movement.model";
import {Page} from "@shared/models/page.model";

@Injectable({providedIn: 'root'})
export class WarehouseMovementService {

  basUrl = environment.SERVER_API + '/warehouse_movements';

  constructor(private http:HttpClient){ }

  createOutput(body:OutputMovement):Observable<OutputMovement>{
    return this.http.post<OutputMovement>(`${this.basUrl}/outputs`, body);
  }

  createInput(body:InputMovement):Observable<InputMovement>{
    return this.http.post<InputMovement>(`${this.basUrl}/inputs`, body);
  }

  updateOutput(id: number, body:OutputMovement):Observable<OutputMovement>{
    return this.http.put<OutputMovement>(`${this.basUrl}/outputs/${id}`, body);
  }

  updateInput(id: number, body:InputMovement):Observable<InputMovement>{
    return this.http.put<InputMovement>(`${this.basUrl}/inputs/${id}`, body);
  }

  getAllWarehouseMovements(page:number, size:number, type?: WarehouseMovementType): Observable<Page<WarehouseMovement>>{
    let params = new HttpParams()
      .set('page', page)
      .set('size', size)

    if (type) params = params.set('type', type)

    return this.http.get<Page<WarehouseMovement>>(this.basUrl, { params });
  }

  getOutputById(id:number): Observable<OutputMovement>{
    return this.http.get<OutputMovement>(`${this.basUrl}/outputs/${id}`);
  }

  getInputById(id:number): Observable<InputMovement>{
    return this.http.get<InputMovement>(`${this.basUrl}/inputs/${id}`);
  }

  verifyCorrespondingInput(goodReference: string): Observable<HttpResponse<null>> {
    return this.http.get<null>(`${this.basUrl}/outputs/verify`, { params: {goodReference}, observe: "response"})
  }
}
