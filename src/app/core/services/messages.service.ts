import {Injectable, Injector} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MessageService} from "primeng/api";

/**
 * Message service allowing to display toasts
 */
@Injectable({providedIn: 'root'})
export class MessagesService {

  private translateService: TranslateService;

  constructor(private messageService: MessageService, private injector: Injector) {
  }

  /**
   * Green toast
   * Take one parameter that is an i18n string on this format :
   * {
   *  title: '',
   *  content: ''
   * }
   */
  showSuccess(detail: string, title: string | null = null) {
    this.showMessage("success", detail, title ?? "GENERAL.SUCCESS");
  }

  /**
   * Red Toast
   * Take one parameter that is an i18n string on this format :
   * {
   *  content: '',
   *  title: ''
   * }
   */
  showError(detail: string, title: string | null = null) {
    this.showMessage("error", detail, title ?? "GENERAL.ERROR");
  }

  /**
   * Orange toast
   * Take one parameter that is an i18n string on this format :
   * {
   *  title: '',
   *  content: ''
   * }
   */
  showWarning(detail: string, title: string | null = null) {
    this.showMessage("warning", detail, title ?? "GENERAL.WARNING");
  }

  showMessage(severity: string, detail: string, title: string) {
    this.translateService = this.injector.get(TranslateService);
    this.translateService.get([title, detail]).subscribe((translation) => {
      this.messageService.add({
        severity: severity,
        summary: translation[title],
        detail: translation[detail]
      });
    });
  }
}
