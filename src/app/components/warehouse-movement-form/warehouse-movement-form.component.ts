import {Component} from '@angular/core';
import {ButtonModule} from "primeng/button";
import {DropdownModule} from "primeng/dropdown";
import {InputSwitchModule} from "primeng/inputswitch";
import {InputTextModule} from "primeng/inputtext";
import {MultiSelectModule} from "primeng/multiselect";
import {Location, NgIf} from "@angular/common";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CrudEnum} from "@core/enums/crud.enum";
import {RoutesEnum} from "@core/enums/routes.enum";
import {MessagesService} from "@core/services/messages.service";
import {WarehouseMovementType} from "@shared/enums/warehouse-movement-type.enum";
import {WarehouseMovementService} from "@shared/services/warehouse-movement.service";
import {InputMovement} from "@shared/models/input.model";
import {OutputMovement} from "@shared/models/output.model";
import {CustomStatus} from "@shared/enums/custom-status.enum";
import {DocumentRefType} from "@shared/enums/document-ref-type.enum";
import {ReferenceType} from "@shared/enums/reference-type.enum";
import {PlaceCode} from "@shared/enums/place-code.enum";
import {SharedModule} from "@shared/shared.module";
import {WarehouseMovement} from "@shared/models/warehouse-movement.model";
import {Good} from "@shared/models/good.model";
import {TranslateModule} from "@ngx-translate/core";

@Component({
  selector: 'rapidcargo-warehouse-movement-form',
  standalone: true,
  imports: [
    ButtonModule,
    DropdownModule,
    InputSwitchModule,
    InputTextModule,
    MultiSelectModule,
    NgIf,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule
  ],
  templateUrl: './warehouse-movement-form.component.html',
  styleUrl: './warehouse-movement-form.component.scss'
})
export class WarehouseMovementFormComponent {
  crudMode: CrudEnum;
  movementType: WarehouseMovementType;
  title: string;

  private id: number;
  loaded = false;

  form: FormGroup;

  placeCodes = Object.values(PlaceCode); // TODO: load back datas by api call
  customStatuses = Object.values(CustomStatus); // TODO: load back datas by api call
  documentRefTypes = Object.values(DocumentRefType); // TODO: load back datas by api call
  referenceTypes = Object.values(ReferenceType); // TODO: load back datas by api call

  protected readonly CrudEnum = CrudEnum;

  constructor(public location: Location,
              private formBuilder: FormBuilder,
              private warehouseMovementService: WarehouseMovementService,
              private messageService: MessagesService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.data.subscribe(data => {
      this.title = data["pageTitle"]
      this.crudMode = data["crudMode"]
      this.movementType = data["movementType"]

      this.route.params.subscribe(params => {
        this.id = params["id"]

        switch (this.crudMode) {
          case CrudEnum.CREATE:
            this.initCreationForm();
            break
          case CrudEnum.EDIT:
          case CrudEnum.VIEW:
            this.initEditionForm();
            break
        }
      });
    })
  }

  get goods(): FormArray {
    return this.form.get('goods') as FormArray;
  }

  private initEditionForm() {
    switch (this.movementType) {
      case WarehouseMovementType.INPUT:
        this.warehouseMovementService.getInputById(this.id).subscribe(input => {
          this.initInputForm(input);
          this.loaded = true;
        })
        break
      case WarehouseMovementType.OUTPUT:
        this.warehouseMovementService.getOutputById(this.id).subscribe(output => {
          this.initOutputForm(output);
          this.loaded = true;
        })
        break
    }
  }

  private initCreationForm() {
    switch (this.movementType) {
      case WarehouseMovementType.INPUT:
        this.initInputForm();
        break
      case WarehouseMovementType.OUTPUT:
        this.initOutputForm();
        break
    }

    if (this.crudMode == CrudEnum.VIEW) this.form.disable()
  }

  private initInputForm(warehouseMovement?: InputMovement) {
    this.form = this.formBuilder.group({
      type: [WarehouseMovementType.INPUT, [Validators.required]],
      declaredIn: this.formBuilder.group({
        code: ['CDGRC1', [Validators.required]],
        label: ['RapidCargo CDG', [Validators.required]]
      }),
      from: this.formBuilder.group({
        code: [warehouseMovement?.from.code ?? '', [Validators.required]],
        label: [warehouseMovement?.from.label ?? '', [Validators.required]]
      }),
      goods: this.formBuilder.array([]),
      customStatus: [warehouseMovement?.customStatus ?? '', [Validators.required]],
    });

    this.initGoodsFormArray();
  }

  private initOutputForm(warehouseMovement?: OutputMovement) {
    this.form = this.formBuilder.group({
      type: [WarehouseMovementType.OUTPUT, [Validators.required]],
      declaredIn: this.formBuilder.group({
        code: ['CDGRC1', [Validators.required]],
        label: ['RapidCargo CDG', [Validators.required]]
      }),
      to: this.formBuilder.group({
        code: [warehouseMovement?.to.code ?? '', [Validators.required]],
        label: [warehouseMovement?.to.label ?? '', [Validators.required]]
      }),
      goods: this.formBuilder.array([]),
      customStatus: [warehouseMovement?.customStatus ?? '', [Validators.required]],
      documentRef: this.formBuilder.group({
        type: [warehouseMovement?.documentRef.type ?? '', [Validators.required]],
        reference: [warehouseMovement?.documentRef.reference ?? '', [Validators.required]]
      }),
    })

    this.initGoodsFormArray();
  }

  private initGoodsFormArray(warehouseMovement?: WarehouseMovement) {
    switch (this.crudMode) {
      case CrudEnum.CREATE:
        this.addGoodControl();
        break
      case CrudEnum.VIEW:
      case CrudEnum.EDIT:
         warehouseMovement?.goods.map(good => this.addGoodControl(good));
        break
    }
  }

  private addGoodControl(good?: Good) {
    const goodControl = this.formBuilder.group({
      reference: this.formBuilder.group({
        code: [good?.reference.code ?? '', [Validators.required, this.codeLengthValidator(this.goods.length)]],
        type: [good?.reference.type ?? '', [Validators.required]]
      }),
      amount: this.formBuilder.group({
        quantity: [good?.amount.quantity ?? 0, [Validators.required]],
        weight: [good?.amount.weight ?? 0, [Validators.required]],
      }),
      totalRefAmount: this.formBuilder.group({
        quantity: [good?.totalRefAmount.quantity ?? 0, [Validators.required, this.totalValidator(this.goods.length, "quantity")]],
        weight: [good?.totalRefAmount.weight ?? 0, [Validators.required, this.totalValidator(this.goods.length, "weight")]],
      }),
      description: [good?.description ?? '', [Validators.required]]
    })

    this.goods.push(goodControl)
  }

  onSubmit(): void {
    if (this.form.valid) {
      switch (this.movementType) {
        case WarehouseMovementType.OUTPUT:
          this.submitOutputForm()
          break;
        case WarehouseMovementType.INPUT:
          this.submitInputForm()
          break;
      }
    }
  }

  private submitOutputForm() {
    this.warehouseMovementService.verifyCorrespondingInput(this.goods.value[0].reference.code).subscribe(response => {
      if (response.status === 204) {
        switch (this.crudMode) {
          case CrudEnum.CREATE:
            this.warehouseMovementService.createOutput(this.form.getRawValue()).subscribe(() => {
              this.router.navigate([`/${RoutesEnum.ROOT}/${this.id}/view`]);
              this.messageService.showSuccess("WAREHOUSE_MOVEMENT.FORM.SUCCESS.OUTPUT")
            });
            break;

          case CrudEnum.EDIT:
            this.warehouseMovementService.updateOutput(this.id, this.form.getRawValue()).subscribe(() => {
              this.router.navigate([`/${RoutesEnum.OUTPUT}/${this.id}/view`]);
              this.messageService.showSuccess("WAREHOUSE_MOVEMENT.FORM.SUCCESS.OUTPUT")
            });
            break;
        }
      }
    })
  }

  private submitInputForm() {
    switch (this.crudMode) {
      case CrudEnum.CREATE:
        this.warehouseMovementService.createInput(this.form.getRawValue()).subscribe(() => {
          this.router.navigate([`/${RoutesEnum.ROOT}/${this.id}/view`]);
          this.messageService.showSuccess("WAREHOUSE_MOVEMENT.FORM.SUCCESS.INPUT")
        });
        break;
      case CrudEnum.EDIT:
        this.warehouseMovementService.updateInput(this.id, this.form.getRawValue()).subscribe(() => {
          this.router.navigate([`/${RoutesEnum.INPUT}/${this.id}/view`]);
          this.messageService.showSuccess("WAREHOUSE_MOVEMENT.FORM.SUCCESS.INPUT")
        });
        break;
    }
  }

  protected readonly WarehouseMovementType = WarehouseMovementType;

  private codeLengthValidator(index: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return (this.goods.at(index)?.get('reference')?.get('type')?.value === "AWB" && control.value.length !== 11)
        ? { forbiddenLength: { value: control.value } }
        : null;
    };
  }

  private totalValidator(index: number, field: string) {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.value < this.goods.at(index)?.get('amount')?.get(field)?.value
        ? { forbiddenTotal: { value: control.value } }
        : null;
    };
  }
}
