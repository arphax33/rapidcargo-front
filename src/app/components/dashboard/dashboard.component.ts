import { ChangeDetectionStrategy, Component } from '@angular/core';
import {ButtonModule} from "primeng/button";
import {RoutesEnum} from "@core/enums/routes.enum";
import {RouterLink} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";

@Component({
  selector: 'rapidcargo-dashboard',
  standalone: true,
  imports: [
    TranslateModule,
    ButtonModule,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  name: string;
  protected readonly RoutesEnum = RoutesEnum;
}
