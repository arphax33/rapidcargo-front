import {WarehouseMovementType} from "@shared/enums/warehouse-movement-type.enum";
import {Good} from "@shared/models/good.model";
import {Place} from "@shared/models/place.model";

export class WarehouseMovement {
  id: string;
  type: WarehouseMovementType;
  declaredIn: Place;
  from: Place;
  to: Place;
  goods: Good[];
  customStatus: string;
  movementTime: Date;
  messageTime: Date;

  constructor(
    id: string,
    type: WarehouseMovementType,
    from: Place,
    to: Place,
    goods: Good[],
    customStatus: string,
    movedAt: Date
  ) {
    this.id = id;
    this.type = type;
    this.from = from;
    this.to = to;
    this.goods = goods;
    this.customStatus = customStatus;
    this.movementTime = movedAt;
  }
}
