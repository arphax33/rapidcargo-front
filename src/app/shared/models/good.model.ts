export class Good {
  id?: number;
  reference: Reference;
  amount: Amount;
  totalRefAmount: Amount;
  description: string;
}

export class Amount {
  quantity: number;
  weight: number;
}

export class Reference {
  type: string;
  code: string;
}
