import {RoutesEnum} from "@core/enums/routes.enum";
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WarehouseMovementFormComponent} from "@components/warehouse-movement-form/warehouse-movement-form.component";
import {CrudEnum} from "@core/enums/crud.enum";
import {DashboardComponent} from "@components/dashboard/dashboard.component";
import {WarehouseMovementType} from "@shared/enums/warehouse-movement-type.enum";
import {HistoryComponent} from "@components/history/history.component";

const ROUTES: Routes = [
  // DASHBOARD --------
  {
    path: `${RoutesEnum.ROOT}`,
    component: DashboardComponent,
    data: {pageTitle: 'DASHBOARD.TITLE'}
  },

  // HISTORY --------

  {
    path: `${RoutesEnum.HISTORY}`,
    component: HistoryComponent,
    data: {pageTitle: 'HISTORY.TITLE'}
  },

  // INPUT --------

  {
    path: `${RoutesEnum.OUTPUT}/${RoutesEnum.CREATE}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'OUTPUT.CREATE.TITLE', crudMode: CrudEnum.CREATE, movementType: WarehouseMovementType.OUTPUT}
  },
  {
    path: `${RoutesEnum.OUTPUT}/:id/${RoutesEnum.EDIT}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'OUTPUT.EDIT.TITLE', crudMode: CrudEnum.EDIT, movementType: WarehouseMovementType.OUTPUT}
  },
  {
    path: `${RoutesEnum.OUTPUT}/:id/${RoutesEnum.VIEW}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'OUTPUT.VIEW.TITLE', crudMode: CrudEnum.VIEW, movementType: WarehouseMovementType.OUTPUT}
  },

  // OUTPUT --------

  {
    path: `${RoutesEnum.INPUT}/${RoutesEnum.CREATE}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'INPUT.CREATE.TITLE', crudMode: CrudEnum.CREATE, movementType: WarehouseMovementType.INPUT}
  },
  {
    path: `${RoutesEnum.INPUT}/:id/${RoutesEnum.EDIT}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'INPUT.EDIT.TITLE', crudMode: CrudEnum.EDIT, movementType: WarehouseMovementType.INPUT}
  },
  {
    path: `${RoutesEnum.INPUT}/:id/${RoutesEnum.VIEW}`,
    component: WarehouseMovementFormComponent,
    data: {pageTitle: 'INPUT.VIEW.TITLE', crudMode: CrudEnum.VIEW, movementType: WarehouseMovementType.INPUT}
  },

  // DEFAULT --------

  {path: '**', redirectTo: RoutesEnum.ROOT, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, { useHash: true, anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
