import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { CommonModule } from '@angular/common';
import {SkeletonDirective} from "@core/directives/auto-skeleton.directive";

@NgModule({
  imports: [
    NgxPermissionsModule.forChild(),
    NgxWebstorageModule.forRoot(),
    CommonModule
  ],
  declarations: [SkeletonDirective],
  providers: [],
  exports: [
    CommonModule,
    NgxWebstorageModule,
    SkeletonDirective
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
