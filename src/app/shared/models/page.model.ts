export class Page<T>{
  content: T[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number
  pageable: {
    pageNumber: number;
    pageSize: number;
    offset: number;
  };
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
  unpaged: boolean;
  size: number;
  totalElements: number;
  totalPages: number;
}
