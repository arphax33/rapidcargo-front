import {Component, OnInit} from '@angular/core';
import {TableLazyLoadEvent, TableModule} from "primeng/table";
import {WarehouseMovement} from "@shared/models/warehouse-movement.model";
import {WarehouseMovementService} from "@shared/services/warehouse-movement.service";
import {Page} from "@shared/models/page.model";
import {TranslateModule} from "@ngx-translate/core";
import {SharedModule} from "@shared/shared.module";
import {WarehouseMovementType} from "@shared/enums/warehouse-movement-type.enum";

@Component({
  selector: 'rapidcargo-history',
  standalone: true,
  imports: [
    TableModule,
    SharedModule,
    TranslateModule
  ],
  templateUrl: './history.component.html',
  styleUrl: './history.component.scss'
})
export class HistoryComponent implements OnInit {
  warehouseMovements: Page<WarehouseMovement>;

  loading: boolean = false;
  pageSize: number = 50;

  constructor(private warehouseMovementService: WarehouseMovementService) {
  }

  ngOnInit() {
    this.loading = true;
    this.warehouseMovementService.getAllWarehouseMovements(0, this.pageSize)
      .subscribe(datas => {
        this.warehouseMovements = datas
        this.loading = false
      });
  }

  loadPage(event: TableLazyLoadEvent) {
    if (event?.first) {
      this.warehouseMovementService.getAllWarehouseMovements(Math.floor(event.first / this.pageSize), this.pageSize)
        .subscribe(datas => {
          this.warehouseMovements = datas
          this.loading = false
        });
    }
  }

  protected readonly WarehouseMovementType = WarehouseMovementType;
}
