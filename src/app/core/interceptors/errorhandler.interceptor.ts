import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { MessagesService } from '@core/services/messages.service';
import { StatusCodes } from 'http-status-codes';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * Intercepteur d'erreurs HTTP
 */
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(
    private readonly messageService: MessagesService
  ) {
  }

  intercept(request: HttpRequest<undefined>, next: HttpHandler): Observable<HttpEvent<undefined>> {
    return next.handle(request).pipe(
      tap(() => {
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            case StatusCodes.UNAUTHORIZED:
              this.messageService.showError('GENERAL.ERRORS.UNAUTHORIZED');
              break;
            case StatusCodes.BAD_REQUEST:
              this.messageService.showError(err.error.message);
              break;
            case StatusCodes.INTERNAL_SERVER_ERROR:
              this.messageService.showError('GENERAL.ERRORS.DEFAULT');
              break;
            case StatusCodes.SERVICE_UNAVAILABLE:
            case StatusCodes.GATEWAY_TIMEOUT:
              this.messageService.showError('GENERAL.ERRORS.UNAVAILABLE');
              break;
            default:
              break;
          }
        })
    );
  }
}
