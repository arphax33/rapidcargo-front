import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {MessageService} from "primeng/api";

@NgModule({
  imports: [HttpClientModule],
  declarations: [],
  providers: [
    MessageService
  ],
  exports: [],
})
export class CoreModule { }
