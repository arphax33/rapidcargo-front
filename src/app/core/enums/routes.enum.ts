export enum RoutesEnum {
  ROOT = '',
  INPUT = 'input',
  OUTPUT = 'output',
  HISTORY = 'history',

  CREATE = 'create',
  EDIT = 'edit',
  VIEW = 'view',
}
